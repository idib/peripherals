; резадент

.model tiny
segm segment para public 'code'
assume cs:segm, ds:segm, ss:segm, es:segm
org 100h


start: jmp beg

m1 db ':','$' ;приглашение ко вводу символов 
nl db 13,10,'$' ;переход на новую строку
f db 0 ;флажок для определения временного
;интервала
tic db 0 ;переменная для подсчета количества тиков
oldadr dd ? ;адрес старого обработчика int 1Ch

old09 dd ? ; адрес для старого обработчика

InsScanCode     equ     52h
DelScanCode     equ     52h

; Bits for the various modifier keys

KbdFlags        equ     <byte ptr ds:[17h]>
CtrlBit         equ     4
AltBit          equ     8

sign dw 0fedch



mytime proc far 		;наш обработчик прерывания 1Ch
inc cs:tic 				;при входе в обработчик увеличим
						;количество тиков
cmp cs:tic,91 			;сравним количество тиков с числом
						; 91, соответствующим 5 секундам
jne quit 				;если 5 с не прошло, то выход из ПОП
mov cs:tic,0 			;иначе обнулим количество тиков,чтобы
						;можно было отсчитать следующие 5 с
inc cs:f 				;увеличим значение f (теперь оно
						; равно 1,2 или 3)
cmp cs:f,3				;сравним f с 1 для определения промежутка
jna quit
mov cs:f, 0
quit: iret
mytime endp


Int09 proc far
sti 					;разрешим прерывания более высокого приоритета
push ax 				;сохраним в стеке изменяемый регистр
in      al, 60h         ;Get keyboard data.
cmp     al, DelScanCode ;Is it the delete key?
jne TryBlock

	push bx
	push di
	push es
	;sti ;разрешить прерывания
	pushf
	 ;для iret в системном прерывании
	call cs:[old09] ;вызов системного прерывания
	mov ax,40h
	 ;устанавливаем регистр es на область
	;данных BIOS
	mov es,ax
	mov di,es:[1ch];получаем указатель на голову
	cmp di,01Eh
	 ;он указывает на начало буфера?
	jne minus2
	 ;если нет, то для доступа к только
	mov di,03Ch
	 ;что помещенным в буфер scan и ascii-
	;кодам необходимо отнять 2 от нового “хвоста”
	jmp el
	 ;иначе старый “хвост” указывал на
	minus2: sub di,2
	 ;конец буфера, что и запишем в di
	;и перейдем на el.
	el: mov ax,es:[di] ;cчитаем scan код из буфера клавиатуры
	mov bh, es:[17h]
	and bh, 00001100b
	cmp bh, 00001100b
	jne pass1


	cmp cs:f, 1
	ja pp2
	mov al, 31h
	mov es:[di], ax
	jmp pass1
	pp2:
	mov al, 32h
	mov es:[di], ax

	;cli ;запретить прерывания

	pass1:
	mov al,20h
	 ;выдаем сигнал о завершении
	out 20h,al
	 ;аппаратного прерывания
	pop es
	 ;восстанавливаем регистры из стека
	pop di
	pop bx
	pop ax
	iret

TryBlock:
cmp cs:f, 1
jbe ingnoreSpace
cmp cs:f, 3
jbe ingnoreNum

ingnoreSpace:
cmp al,39h ;сравним полученный код со scan-кодом пробела
je blok ;если нажат пробел, то на метку blok
jmp pass ;если нет, то на метку pass

ingnoreNum:
cmp al, 02h
jb pass
cmp al, 0bh
ja pass

blok: in al,61h ;посылаем подтверждение
mov ah,al ;о считывании scan-кода
or al,80h ;из порта 60h
out 61h,al ;в порт 61h
mov al,ah
out 61h,al
cli ;запретим прерывания
mov al,20h ;пошлем контроллеру прерываний
out 20h,al ;сигнал EOI
pop ax ;восстановим из стека ax Ц
iret ;завершим обработчик прерываний
pass:pop ax ;восстановим ax
jmp cs:[old09];на выполнение старого обработчика
int09 endp ;конец процедуры


beg:
mov ax,3509h				; проверим адрес обработчика int 09h
int 21h
mov ax,es:[bx-2]			; это наш обработчик в памяти?
cmp ax,cs:sign
je outmem 					; если наш – на выгрузку

mov word ptr old09,bx 		; системный – сохраним его
mov word ptr old09+2,es 	; адрес в двойном слове old09
mov bx,cs
mov ds,bx 					; установим ds на наш сегмент
mov dx,offset int09 		; в dx поместим адрес нашей ISR
mov ax,2509h				; и запишем адреса в вектор
int 21h						; прерываний
mov		ax,351ch 			
int 	21h
mov 	word ptr oldadr,bx 	
mov 	word ptr oldadr+2,es
mov 	dx,offset mytime
mov		ax,251ch
int		21h
lea dx, beg
int 27h						; оставим резидентно

outmem:		 				; выгрузка
mov dx, word ptr es:old09
mov bx, word ptr es:old09+2
mov ds,	bx
mov ax,	2509h 				; запись в вектор прерываний
int 21h 					; адреса стандартного обработчика
mov dx, word ptr es:oldadr
mov bx, word ptr es:oldadr+2
mov ds,	bx
mov ax,	251ch 				; запись в вектор прерываний
int 21h 					; адреса стандартного обработчика

push es
mov es,	es:[2Ch] 			; выгрузка окружения
mov ah,	49h
int 21h
pop es
mov ah,	49h					; выгрузка программы
int 21h
int 20h
segm ends
end start
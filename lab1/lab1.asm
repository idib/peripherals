global _start

section .data

prompt db "Enter size (max 10)", 13, 10
lenprompt     equ $ - prompt

prompt1 db  13, 10
lenprompt1     equ $ - prompt1

prompt1 db  "---" 13, 10
lenprompt1     equ $ - prompt1

section .bss
	arra:  	resd	100
	arra1:	resd	100
	arra2:	resd	100

	N:	resd	1

	temptext 	resd 	1
	lentemptext     equ $ - temptext     
	tempint 	resd 	1
	negative resb 1


section .text

; перевод из числа в строку
; вход - eax
; выход - temptext
itos:
	cmp eax, 0
	mov [temptext], byte '0'
	mov [temptext+4], byte 10
	je end
	  mov  ebx, 10
	  mov  ecx, 0
	  test eax, eax
	  jns  lp
	  imul eax, -1
	  mov  byte [temptext], '-'
	lp:
	  cmp  eax, 0
	  je   en

	  cdq
	  idiv ebx
	  inc  ecx

	  add  edx, '0'
	  push rdx
	  jmp  lp

	en:
	  cmp  byte [temptext], '-'
	  jne  fromstk
	  inc  eax

	fromstk:
	  pop  rdx
	  mov  [temptext + 1 * eax],edx
	  inc  eax
	  dec  ecx

	  cmp  ecx, 0
	  jnz  fromstk

	end:
	  mov  byte[temptext + 1 * eax], 10 ; перевод строки
ret


; переводим строку в число 
; вход  edx 
; выход eax
stoi: 
  xor   eax, eax ; результат в eax
  mov   ebx, 1
  top:
  movzx ecx, byte [edx] ;берем символ. movzx добивает оставшееся нулями
  inc   edx 
  cmp   ecx, '9'
  ja    done
  cmp   ecx, '-'
  je    minus
  cmp   ecx, '0' 
  jb    done
  sub   ecx, '0' ; делаем из символа цифру
  imul  eax, 10 
  add   eax, ecx
  jmp   top
  
  minus:
  mov   ebx, -1
  jmp   top
  done:
  imul  eax, ebx
ret


read:
	push rax
	push rbx
	push rdx
	mov eax, 3
	mov ebx, 0
	mov ecx, temptext
	mov edx, lentemptext
	int 80h
	mov edx,temptext
	call stoi
	mov ecx, eax



	pop rdx
	pop rbx
	pop rax
ret

write:
	push rax
	push rbx
	mov eax, 4
	mov ebx, 1
	int 80h
	pop rbx
	pop rax
ret

dread:
	push rax
	push rbx
	push rcx
	push rdx

	call read
	add rax, r13
	mov [eax], ecx
	
	pop rdx
	pop rcx
	pop rbx
	pop rax
ret

dMat:
	push rax
	push rbx
	push rcx
	push rdx

	mov eax, [r13 + eax*4]

	call itos
	mov ecx, temptext
	mov edx, lentemptext
	call write
	pop rdx
	pop rcx
	pop rbx
	pop rax
ret

;r13 исх
;r14 1
;r15 2
copy:
	push r13
	push r14
	push r15
	mov r12,[r13 + eax*4]

	cmp ebx, ecx
	jb leb1

	mov [r14  + eax*4], r12
	leb1:
	cmp ebx, ecx
	ja leb2
	mov [r15 + eax*4], r12
	pop r15
	pop r14
	pop r13
leb2:
ret



%macro work 1
	mov ecx, [N]
	mov eax, [N]
	mul cx
	%%lp1:
		mov ebx, [N]
		%%lp2:
			call %1
			dec eax
			dec ebx
			jnz %%lp2
	loop %%lp1
%endmacro

%macro mat 3
	mov ecx, prompt
	mov edx, lenprompt
	call write
	call read
	mov [N], ecx
	mov r13, %1
	mov r14, %2
	mov r15, %3
	work dread
	mov ecx, prompt
	mov edx, lenprompt
	call write
	work copy
	work dMat
	mov r13, r14
	work dMat
	mov r13, r15
	work dMat
%endmacro

_start:
	mat arra, arra1, arra2
	mat arra, arra1, arra2
	mov  eax, 1
	mov  ebx, 0
	int  80h
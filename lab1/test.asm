global _start

section .data

prompt db "Enter size (max 10)", 13, 10
lenprompt     equ $ - prompt     

section .bss
	arra:  	resd	100
	arra1:	resd	100
	arra2:	resd	100
	arrb:  	resd	100
	arrb1:	resd	100
	arrb2:	resd	100

	N:	resd	1

	temptext 	resb 	3
	lentemptext     equ $ - temptext     
	tempint 	resd 	1


section .text

read:
	push rax
	push rbx
	push rdx
	mov eax, 3
	mov ebx, 0
	mov ecx, temptext
	mov edx, lentemptext
	int 80h
	;call stoi
	pop rdx
	pop rbx
	pop rax
ret

write:
	push rax
	push rbx
	mov eax, 4
	mov ebx, 1
	int 80h
	syscall
	pop rbx
	pop rax
ret

%macro work 3
	mov ecx, [N]
	lp1:
		mov r10, [N]
		lp2:
			call 
			dec r10
			jnz lp2
	loop lp1
%endmacro


_start:
	mov rax, write
	call rax
	call write



	mov  eax, 1
	mov  ebx, 0
	int  80h



stdin     equ 0
sys_exit  equ 1
stdout    equ 1
sys_read  equ 3
sys_write equ 4

section .bss
  m      resd 1
  n      resd 1
  numLen equ $-m

  a1     resd 100
  a2     resd 100
  a3     resd 100
section .data

  msg1:    db 'Введите размеры матрицы: '
  msg1Len: equ $-msg1
  msg2:    db '--------',10
  msg2Len: equ $-msg2

  msg3:    db ' ',10
  msg3Len: equ $-msg3

; считывание матрицы 
%macro readto 3 
  mov  eax, [%2]
  push eax
  xor  esi,esi
  mov  eax, dword [%2]
  mov  ebx, dword [%3]
  imul ebx
  mov  edi, eax

  %%loop:
     cmp  esi, edi
     je   %%done
     
     mov  ecx, %2
     mov  edx, numLen
     call ReadText
     mov  edx, %2 ;
     call atoi

     mov  dword[%1 + 4 * esi],eax
     inc  esi
     jmp  %%loop
%%done:

pop eax
mov [%2], eax
%endmacro


; вывод матрицы 
%macro outto 3
  mov  eax, dword [%2]
  mov  ebx, dword [%3]
  imul ebx
  mov  edi, eax
  mov  esi,0
  
  %%loop:
   cmp  esi, edi
   je   %%done
   mov  eax, dword[%1 + 4 * esi]
   
   call itoa
   mov  ecx, n
   mov  edx, msg1Len
   call DisplayText

   inc esi
 jmp %%loop
%%done:

%endmacro


; работа с матрицей 
%macro mtrx 3
 
xor eax,eax
;mov eax,0
mov ecx, 0
mov edx, dword [%2]
%%loop1:
  inc  ecx
  cmp  ecx,edx
  je   %%done
  test ecx,1
  jz   %%loop1 ; четные числа нас не интересуют
  mov  esi, dword [%3]
  %%loop2:
    dec  esi
    ;push ecx
    ;push esi
    push edx
    push eax
    mov  eax, esi
    imul edx
    add  eax,ecx
    mov  ebx, [%1 + (eax) * 4]
    pop  eax
    pop  edx
    ;pop esi
    ;pop ecx
    add  eax, ebx
    cmp  esi, 0
  jne %%loop2
jmp %%loop1
%%done:
%endmacro

; сюда положил повторяющие куски при вводе 
%macro getall 2
 ; выводим приглашающее сообщение
  mov  ecx, msg1
  mov  edx, msg1Len
  call DisplayText
  
  ; вводим размер первой матрицы
  mov  ecx, %1
  mov  edx, numLen
  call ReadText
  
  mov  edx, %1 ;
  call atoi
  mov [%1], eax 

  mov  ecx, msg1
  mov  edx, msg1Len
  call DisplayText

  mov  ecx, %2
  mov  edx, numLen
  call ReadText
  
  mov  edx, %2 ;
  call atoi
  mov [%2],eax 

%endmacro

section .text
  global _start

_start:
  getall m,n
  readto a1,m,n
  
 test:


  mtrx   a1,m,n

  push   eax

  getall m,n
  readto a2,m,n
  mtrx   a2,m,n

  pop    edx
  add    eax, edx
  push   eax

  getall m,n
  readto a3,m,n
  mtrx   a3,m,n

  pop    edx
  add    eax, edx

  mov    ebx, 8
  cdq
  idiv   ebx
  mov    eax, edx

  call   itoa

  mov    ecx, msg2
  mov    edx, msg2Len
  call   DisplayText

  mov    ecx, n
  mov    edx, 2
  call   DisplayText


  mov    ecx, msg3
  mov    edx, msg3Len
  call   DisplayText

;  mov    ecx,  word 0xA
;  mov    edx, 4
;  call   DisplayText

  mov    eax, 1
  mov    ebx, 0
  int    80h

  ; считываем тут
ReadText:
  mov ebx, stdin
  mov eax, sys_read
  int 80h
ret

; выводим тут 
DisplayText:
  mov eax, sys_write
  mov ebx, stdout
  int 80h
ret


; перевод из числа в строку
; вход - eax
; выход - n(сделать в регистре не смог)
itoa:
cmp eax, 0
mov [n], byte '0'
mov [n+4], byte 10
je end
  mov  ebx, 10
  mov  ecx, 0
  test eax, eax
  jns  lp
  imul eax, -1
  mov  byte [n], '-'
lp:
  cmp  eax, 0
  je   en

  cdq
  idiv ebx
  inc  ecx

  add  edx, '0'
  push edx
  jmp  lp

en:
  cmp  byte [n], '-'
  jne  fromstk
  inc  eax

fromstk:
  pop  edx
  mov  [n + 1 * eax],edx
  inc  eax
  dec  ecx

  cmp  ecx, 0
  jnz  fromstk

end:
  mov  byte[n + 1 * eax], 10 ; перевод строки
  ret

; переводим строку в число 
; вход  edx 
; выход eax
atoi: 
  xor   eax, eax ; результат в eax
  mov   ebx, 1
  top:
  movzx ecx, byte [edx] ;берем символ. movzx добивает оставшееся нулями
  inc   edx 
  cmp   ecx, '9'
  ja    done
  cmp   ecx, '-'
  je    minus
  cmp   ecx, '0' 
  jb    done
  sub   ecx, '0' ; делаем из символа цифру
  imul  eax, 10 
  add   eax, ecx
  jmp   top
  
  minus:
  mov   ebx, -1
  jmp   top
  done:
  imul  eax, ebx
 ret

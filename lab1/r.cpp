#include <iostream>

#include <iomanip>
using namespace std;

int mat[100];
int mats1[100];
int mats2[100];

int main(){
	cout << "Enter size (max 10)" << endl;
	int n;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cin >> mat[i * n + j];
		}
	}
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			mats1 [i*n+j] = 0;
			mats2 [i*n+j] = 0;
			if (j>=i)
			mats1 [i*n+j] = mat[i*n+j];
			if (j <= i)
			mats2 [i*n+j] = mat[i*n+j];
		}
	}
	cout << "---" << endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cout << setw(3) << mats1[i * n + j] << ' ';
		}
		cout << endl;
	}
	cout << "---" << endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cout << setw(3) << mats2[i * n + j] << ' ';
		}
		cout << endl;
	}
	cout << "Enter size (max 10)" << endl;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cin >> mat[i * n + j];
		}
	}
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			mats1 [i*n+j] = 0;
			mats2 [i*n+j] = 0;
			if (j>=i)
			mats1 [i*n+j] = mat[i*n+j];
			if (j <= i)
			mats2 [i*n+j] = mat[i*n+j];
		}
	}
	cout << "---" << endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cout << setw(3) << mats1[i * n + j] << ' ';
		}
		cout << endl;
	}
	cout << "---" << endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			cout << setw(3) << mats2[i * n + j] << ' ';
		}
		cout << endl;
	}
}